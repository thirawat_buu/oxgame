
import java.util.Scanner;
import jdk.nashorn.api.tree.BreakTree;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Win 10 Home
 */
public class OXGame {

    static Scanner kb = new Scanner(System.in);

    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static char player = 'X';
    static int count = 0;

    static void showWelcome() {
        System.out.println("Welcom to OX Game");
    }

    static void showTable() {
        System.out.println("   1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(" " + (row + 1));
            for (int column = 0; column < table[row].length; column++) {
                System.out.print(" " + table[row][column]);
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: table at row and col it not empty!!!");
        }
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkX() {
        if (table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X') {
            isFinish = true;
            winner = player;

        } else if (table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X') {
            isFinish = true;
            winner = player;

        } else if (table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O') {
            isFinish = true;
            winner = player;

        } else if (table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O') {
            isFinish = true;
            winner = player;

        }

    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (isFinish == true) {
            System.out.println(winner + " Win!!!");
        } else if (count >= 9) {
            isFinish = true;
            System.out.println("Draw!!");

        }
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkX();
    }

    static void showBye() {
        System.out.println("Bye bye . . . .");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            count++;
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
            showResult();
        } while (!isFinish);
        showBye();

    }
}
